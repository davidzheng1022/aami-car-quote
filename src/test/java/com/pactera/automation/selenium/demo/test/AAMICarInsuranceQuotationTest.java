package com.pactera.automation.selenium.demo.test;

import com.pactera.adm.automation.AbstractSelenium;
import com.pactera.adm.automation.annotation.Page;
import com.pactera.adm.automation.annotation.SeleniumConfig;
import com.pactera.adm.automation.annotation.TargetURL;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Created by David on 10/18/15.
 */
@TargetURL("https://www.aami.com.au")
@SeleniumConfig("selenium.properties")
public class AAMICarInsuranceQuotationTest extends AbstractSelenium
{
	@Page
	private EntryPage entryPage;

	@Page
	private FindYourCarPage findYourCarPage;

	@Page
	private CarFeaturePage carFeaturePage;

	@Page
	private CarDetailPage carDetailPage;

	@Test
	public void testSuccessScenario()
	{
		entryPage.chooseCarInsurance();
		entryPage.switchToPopUpWindow();
		findYourCarPage.selectManufactureYear("2013");
		findYourCarPage.selectCarMake("Mazda");
		findYourCarPage.selectCarModel("Mazda2");
		findYourCarPage.selectTransType("Auto");
		findYourCarPage.selectEngine("4");
		findYourCarPage.selectBodyType("5D Hatchback");
		findYourCarPage.findYourCar();
		findYourCarPage.chooseYourCar(new String[] { "Neo", "MY13" });
		findYourCarPage.clickCarFeatureBtn();
		carFeaturePage.selectModification(false);
		carFeaturePage.clickCarDetailBtn();
		Assert.assertTrue("The expected page should be Car Details Page!",
				carDetailPage.getTitle().contains("Car Details"));
	}

	@Test
	public void testFailedScenario()
	{
		entryPage.chooseCarInsurance();
		entryPage.switchToPopUpWindow();
		findYourCarPage.selectManufactureYear("2013");
		findYourCarPage.selectCarMake("Mazda");
		findYourCarPage.selectCarModel("Mazda2");
		findYourCarPage.selectTransType("Auto");
		findYourCarPage.selectEngine("4");
		findYourCarPage.selectBodyType("5D Hatchback");
		findYourCarPage.findYourCar();
		findYourCarPage.chooseYourCar(new String[] { "Neo", "MY13" });
		findYourCarPage.clickCarFeatureBtn();
		//carFeaturePage.selectModification(false);
		carFeaturePage.clickCarDetailBtn();
		Assert.assertTrue("The expected page should be Car Details Page!",
				carDetailPage.getTitle().contains("Car Details"));

	}
}
