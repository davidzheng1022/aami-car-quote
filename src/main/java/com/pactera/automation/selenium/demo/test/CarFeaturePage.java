package com.pactera.automation.selenium.demo.test;

import com.pactera.adm.automation.PageContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by David on 10/18/15.
 */
public class CarFeaturePage extends PageContext
{
	@FindBy(css = "#otherAccessoriesModifications1+label")
	private WebElement modificationYesBtn;

	@FindBy(css = "#otherAccessoriesModifications2+label")
	private WebElement modificationNoBtn;

	@FindBy(id = "_eventId_submit")
	private WebElement carDetailBtn;

	public void selectModification(boolean isModified)
	{
		WebElement btn = modificationNoBtn;
		if (isModified)
			btn = modificationYesBtn;

		btn.click();
	}

	public void clickCarDetailBtn() {
		carDetailBtn.click();
	}

}
