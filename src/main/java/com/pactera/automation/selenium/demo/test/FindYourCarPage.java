package com.pactera.automation.selenium.demo.test;

import com.pactera.adm.automation.PageContext;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

/**
 * Created by David on 10/18/15.
 */
public class FindYourCarPage extends PageContext
{
	@FindBy(id = "vehicleYearOfManufactureList")
	private WebElement manufactureYear;

	@FindBy(id = "vehicleMakeList")
	private WebElement makeSelector;

	@FindBy(id = "vehicleModelList")
	private WebElement model;

	@FindBy(id = "vehicleTransmissionList")
	private WebElement transmissionType;

	@FindBy(id = "vehicleNumberOfCylindersList")
	private WebElement engineType;

	@FindBy(id = "vehicleBodyTypeList")
	private WebElement bodyType;

	@FindBy(id = "findcar")
	private WebElement findcarBtn;

	@FindBy(css = "vehicleSearchResultSimplePanel>table")
	private WebElement searchResultTable;

	@FindBy(id = "_eventId_submit")
	private WebElement carFeatureBtn;

	public void selectManufactureYear(String text)
	{
		Select dropdown = new Select(manufactureYear);
		dropdown.selectByVisibleText(text);
	}

	public void selectCarMake(String text)
	{
		Select dropdown = new Select(makeSelector);
		dropdown.selectByVisibleText(text);
	}

	public void selectCarModel(String text)
	{
		Select dropdown = new Select(model);
		dropdown.selectByVisibleText(text);
	}

	public void selectTransType(String text)
	{
		Select dropdown = new Select(transmissionType);
		dropdown.selectByVisibleText(text);
	}

	public void selectEngine(String text)
	{
		Select dropdown = new Select(engineType);
		dropdown.selectByVisibleText(text);
	}

	public void selectBodyType(String text)
	{
		Select dropdown = new Select(bodyType);
		dropdown.selectByVisibleText(text);
	}

	public void findYourCar()
	{
		findcarBtn.click();
	}

	public void clickCarFeatureBtn()
	{
		carFeatureBtn.click();
	}

	public void chooseYourCar(String[] features)
	{
		WebElement tBody = cssSelector("#vehicleList");
		WebElement yourCar = null;
		boolean found = false;
		List<WebElement> rows = tBody.findElements(By.tagName("tr"));
		for (int i =0; rows!= null && i < rows.size(); i++) {
			WebElement row = rows.get(i);
			String text = row.getText();
			for (String keyword : features) {
				if (!(found = StringUtils.containsIgnoreCase(text, keyword)))break;
			}
			if (found) {
				yourCar = row;
			}
		}

		yourCar.findElement(By.tagName("input")).click();
	}
}
