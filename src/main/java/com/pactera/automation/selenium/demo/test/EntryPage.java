package com.pactera.automation.selenium.demo.test;

import com.pactera.adm.automation.PageContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.Iterator;
import java.util.Set;

/**
 * Created by David on 10/18/15.
 */
public class EntryPage extends PageContext
{
	@FindBy(css = ".SelfService-quoteBtn button")
	private WebElement quoteBtn;

	@FindBy(css= "div.SelfService-quoteWrapper ul li:nth-child(1)>a")
	private WebElement carInsuranceLink;

	/**
	 * select the value
	 *
	 * @param text
	 */
	public void chooseCarInsurance()
	{
		quoteBtn.click();
		//cssSelector("div.SelfService-quoteWrapper ul li:nth-child(1)>a").click();
		carInsuranceLink.click();
	}

	public void switchToPopUpWindow() {

		String currentWindow = getDriver().getWindowHandle();
		Set<String> handles = getDriver().getWindowHandles();
		Iterator<String> it = handles.iterator();
		while (it.hasNext())
		{
			if (currentWindow == it.next())
			{
				continue;
			}

			getDriver().switchTo().window(it.next());
			break;
		}
	}
}
